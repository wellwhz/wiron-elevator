package cn.wellwhz;

import cn.nukkit.permission.Permission;
import cn.nukkit.plugin.PluginBase;
import cn.nukkit.utils.Logger;
import cn.wellwhz.listener.PlayerDownstairsListener;
import cn.wellwhz.listener.PlayerUpstairsListener;


public class WIronElevator extends PluginBase {
    public static WIronElevator ins;
    public static Logger logger;

    @Override
    public void onEnable() {
        ins = this;
        logger = this.getLogger();
        logger.info("§c加载成功!");
        this.getServer().getPluginManager().registerEvents(new PlayerUpstairsListener(), this);
        this.getServer().getPluginManager().registerEvents(new PlayerDownstairsListener(), this);
        this.saveDefaultConfig();
        this.getServer().getPluginManager().addPermission(new Permission("wironelevator.use","使用铁块电梯"));
    }
}
