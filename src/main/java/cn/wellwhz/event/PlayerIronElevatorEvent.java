package cn.wellwhz.event;

import cn.nukkit.Player;
import cn.nukkit.event.Cancellable;
import cn.nukkit.event.HandlerList;
import cn.nukkit.event.player.PlayerEvent;

public class PlayerIronElevatorEvent extends PlayerEvent implements Cancellable
{
    private static final HandlerList handlers = new HandlerList();

    public static HandlerList getHandlers()
    {
        return handlers;
    }

    private boolean upstairs;//真为上楼

    public PlayerIronElevatorEvent(Player player,boolean upstairs)
    {
        this.player = player;
        this.upstairs = upstairs;
    }

    public boolean isUpstairs(){
        return upstairs;
    }

    public boolean isDownstairs(){
        return !upstairs;
    }
}
